﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace healthcare_test
{
    public partial class Form1 : Form
    {

        //Create the form
        public Form1()
        {
            InitializeComponent();
        }





        //Global things that need to be accessed by everything
        public static Graphics device;                                                  //Graphics device
        Stopwatch stopwatch = new Stopwatch();                                          //Stopwatch
        public static Random rand_num = new Random();                                   //Random number generator
        public static List<Individual> individuals = new List<Individual>();            //List of every individual in every group
        public Group group_1 = new Group(200, 30);                                      //Create a new instance of Group
        public Group group_2 = new Group(500, 40);                                      //Create a new instance of Group
        public Group group_3 = new Group(1000, 80);                                     //Create a new instance of Group
        public static int population_count;





        //Buttons
        private void btnClick_Click(object sender, EventArgs e)
        {
            if (btnClick.Text == "Start")
            {
                btnClick.Text = "Pause";
                timer1.Enabled = true;
                stopwatch.Start();
            }
            else
            {
                btnClick.Text = "Start";
                timer1.Enabled = false;
                stopwatch.Stop();
            }
        }





        //Classes
        public class Individual
        {
            public Group group;
            public int x;
            public int y;
            public int age;
            public bool alive;
            public void AssignGroup(Group group) //tells an individual what group it's in
            {
                this.group = group;
            }
            public void setLocation(int x, int y)
            {
                this.x = x;
                this.y = y;
            }
            public void setAge(int age)
            {
                this.age = age;
            }
            public void setAlive(bool alive)
            {
                this.alive = alive;
            }
            public int GetSize()
            {
                int size = new int();
                size = this.age/6;
                return size;
            }
            public void Draw()
            {
                    device.FillRectangle(new SolidBrush(group.colour), new Rectangle(this.x, this.y, 2, 2));
            }
            public void Move()
            {
                int newx = rand_num.Next(this.group.nucleus.X - this.group.gravity, this.group.nucleus.X + this.group.gravity); //gravity sets the limit of movement away from the nucleus - need to include the nucleus somewhere in this function so that the agent doesn't go too far away from it
                int newy = rand_num.Next(this.group.nucleus.Y - this.group.gravity, this.group.nucleus.Y + this.group.gravity);
                ValidateMove(newx, newy, group.gravity, group.nucleus, 10, 500, 10, 600);
            }
            public void ValidateMove(int newx, int newy, int gravity, Point nucleus, int minx, int maxx, int miny, int maxy)
            {
                if ((newx > minx) && (newx < maxx) && (newx < nucleus.X + gravity) && (newy > miny) && (newy < maxy) && (newy < nucleus.Y + gravity)) 
                {
                    setLocation(newx, newy);
                }
            }
            public void ChangeGroup(Individual individual, Group old_group, Group new_group)
            {
                //old_group.Remove(individual);
                //new_group.Add(individual);
            }
            public void Die(double chance_of_death)
            {
                if (chance_of_death > 0.5) //change this to be age-dependent
                {
                    this.alive = false;
                    this.group.RemoveIndividual(this);
                }
            }





            public void Reproduce(double chance_of_success)
            {
                    if (chance_of_success > 0.5)
                    {
                        this.group.AddIndividual(new Individual(this.group, 0));
                    }
            }
            public Individual(Group group, int age)
            {
                AssignGroup(group);
                this.age = age;
                x = rand_num.Next(group.nucleus.X - group.gravity, group.nucleus.X + group.gravity);
                y = rand_num.Next(group.nucleus.Y - group.gravity, group.nucleus.Y + group.gravity);
                this.setLocation(x, y);
                this.setAge(rand_num.Next(0, 60));
                this.setAlive(true);
                individuals.Add(this);                
            }
        }



        

        public class Group
        {
            public List<Individual> members = new List<Individual>();
            public Point nucleus;
            public int gravity;                                                                 //A measure of distance an individual can move (the smaller, the tighter the group)
            public int group_size;
            public Color colour;

            public Point SetNucleus()
            {
                nucleus = new Point(rand_num.Next(0, 500), rand_num.Next(0, 500));
                return nucleus;
            }
            public void AddIndividual(Individual individual)
            {
                members.Add(individual);
                individual.AssignGroup(this);
                population_count++;
            }
            public void RemoveIndividual(Individual individual)
            {
                //members.Remove(individual);
                population_count--;
            }
            public Color SetColour()
            {
                colour = Color.FromArgb(rand_num.Next(256), rand_num.Next(256), rand_num.Next(256));
                return colour;
            }
            public Group(int group_size, int gravity)
            {
                for (int i = 0; i < group_size; i++)
                {
                    AddIndividual(new Individual(this, rand_num.Next(0, 50)));
                }
                this.group_size = group_size;
                nucleus = SetNucleus();
                this.gravity = gravity;
                this.colour = SetColour();
            }
        }





        //Functions
        public void DrawScene()
        {
            foreach (Individual individual in individuals)
            {
                if (individual.alive == true)
                {
                    individual.Draw();
                }
            }
        }





        public void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            device = e.Graphics;
            DrawScene();
        }





        public void UpdateLabels()
        {
            int population_size = group_1.group_size + group_2.group_size + group_3.group_size;
            lblTime.Text = "Time: " + stopwatch.Elapsed;
            lblPop_size.Text = "Initial population size: " + population_size;
            lblPop_count.Text = "Current population: " + population_count;
            lblGroup_1_size.Text = "Group 1 Size: " + group_1.members.Count;
            lblGroup_2_size.Text = "Group 2 Size: " + group_2.members.Count;
            lblGroup_3_size.Text = "Group 3 Size: " + group_3.members.Count;
        }








        //Events (that happen when the timer is running)
        private void timer1_Tick(object sender, EventArgs e) //Intervals: 1000 = 1 second
        {
            UpdateLabels();

            
            foreach (Individual individual in individuals)
            {
                individual.age++;
                individual.Move();
            }
            for (int i = 0; i < group_1.group_size/2; i++) //not sure exactly what is happening here - seems to work for about 5 seconds then stops?
            {
                //var result = group_1.members.OrderBy(item => rand_num.Next()); //shuffles group not sure if this is going to be used? doesn't make a difference to how it runs
                if (group_1.members[i].alive == true)
                {
                    group_1.members[i].Reproduce(rand_num.NextDouble());
                    group_1.members[i].Die(rand_num.NextDouble());
                }

            }
            for (int i = 0; i < group_2.group_size; i++)
            {
                if (group_2.members[i].alive == true)
                {
                    group_2.members[i].Reproduce(rand_num.NextDouble());
                    group_2.members[i].Die(rand_num.NextDouble());
                }
            }
            
            Refresh();
        }







        //END









        //Not yet in use

        /*
        public void reproduce(int age)
        {
        //double probability_success = ((age / 2) - 18) ^ 2 + 75); //this will allow the age to determine probability of success
        double probability_success = 1.0;
        if (probability_success > 0.25)
        {
            for (int i = 0; i < population_count; i++)
            {
                members.Add(new Individual());
                population_count += 1;
            }
        }
        }
        */


        /*
        public class SummaryStats //not sure if a class is right for this
        {
            int mean_age;
            int population_size;
            int birth_rate;
            int death_rate;
            public double getMeanAge(Group group)
            {
                int sum = 0;
                for (int i = 00; i < population_size; i++)
                {
                    sum += group.members[i].age;            //sums ages in silly convoluted way
                }
                mean_age = sum / population_size;
                return mean_age;
            }
        }
        */




        //Unused functions I accidentally created
        //Form_load
        public void Form1_Load(object sender, EventArgs e)
        {

        }
        private void lblTime_Click(object sender, EventArgs e)
        {
            //hello
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //hello
        }

        private void lblPop_count_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}

