﻿namespace healthcare_test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClick = new System.Windows.Forms.Button();
            this.lblPop_size = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblTime = new System.Windows.Forms.Label();
            this.lblPop_count = new System.Windows.Forms.Label();
            this.lblGroup_1_size = new System.Windows.Forms.Label();
            this.lblGroup_2_size = new System.Windows.Forms.Label();
            this.lblGroup_3_size = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClick
            // 
            this.btnClick.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.btnClick.Location = new System.Drawing.Point(685, 72);
            this.btnClick.Name = "btnClick";
            this.btnClick.Size = new System.Drawing.Size(75, 23);
            this.btnClick.TabIndex = 0;
            this.btnClick.Text = "Start";
            this.btnClick.UseVisualStyleBackColor = false;
            this.btnClick.Click += new System.EventHandler(this.btnClick_Click);
            // 
            // lblPop_size
            // 
            this.lblPop_size.AutoSize = true;
            this.lblPop_size.Location = new System.Drawing.Point(642, 182);
            this.lblPop_size.Name = "lblPop_size";
            this.lblPop_size.Size = new System.Drawing.Size(85, 13);
            this.lblPop_size.TabIndex = 2;
            this.lblPop_size.Text = "Population_label";
            this.lblPop_size.Click += new System.EventHandler(this.label1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(642, 211);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(58, 13);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "Time_label";
            this.lblTime.Click += new System.EventHandler(this.lblTime_Click);
            // 
            // lblPop_count
            // 
            this.lblPop_count.AutoSize = true;
            this.lblPop_count.Location = new System.Drawing.Point(642, 260);
            this.lblPop_count.Name = "lblPop_count";
            this.lblPop_count.Size = new System.Drawing.Size(118, 13);
            this.lblPop_count.TabIndex = 6;
            this.lblPop_count.Text = "Population_count_label";
            this.lblPop_count.Click += new System.EventHandler(this.lblPop_count_Click);
            // 
            // lblGroup_1_size
            // 
            this.lblGroup_1_size.AutoSize = true;
            this.lblGroup_1_size.Location = new System.Drawing.Point(642, 285);
            this.lblGroup_1_size.Name = "lblGroup_1_size";
            this.lblGroup_1_size.Size = new System.Drawing.Size(72, 13);
            this.lblGroup_1_size.TabIndex = 7;
            this.lblGroup_1_size.Text = "Group_1_size";
            // 
            // lblGroup_2_size
            // 
            this.lblGroup_2_size.AutoSize = true;
            this.lblGroup_2_size.Location = new System.Drawing.Point(642, 313);
            this.lblGroup_2_size.Name = "lblGroup_2_size";
            this.lblGroup_2_size.Size = new System.Drawing.Size(72, 13);
            this.lblGroup_2_size.TabIndex = 8;
            this.lblGroup_2_size.Text = "Group_2_size";
            this.lblGroup_2_size.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lblGroup_3_size
            // 
            this.lblGroup_3_size.AutoSize = true;
            this.lblGroup_3_size.Location = new System.Drawing.Point(642, 341);
            this.lblGroup_3_size.Name = "lblGroup_3_size";
            this.lblGroup_3_size.Size = new System.Drawing.Size(72, 13);
            this.lblGroup_3_size.TabIndex = 9;
            this.lblGroup_3_size.Text = "Group_3_size";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 670);
            this.Controls.Add(this.lblGroup_3_size);
            this.Controls.Add(this.lblGroup_2_size);
            this.Controls.Add(this.lblGroup_1_size);
            this.Controls.Add(this.lblPop_count);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblPop_size);
            this.Controls.Add(this.btnClick);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClick;
        private System.Windows.Forms.Label lblPop_size;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblPop_count;
        private System.Windows.Forms.Label lblGroup_1_size;
        private System.Windows.Forms.Label lblGroup_2_size;
        private System.Windows.Forms.Label lblGroup_3_size;
    }
}

